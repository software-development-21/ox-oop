/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxprogramoop;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    Scanner sc = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcom() {
        System.out.println("Wlecome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;

            if (table.setRowCol(row, col)) {
                break;
            }

            table.setRowCol(row, col);
            System.out.println("Error: table at row and col is not empty!!!");
        }

    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }
    public void newGame(){
        table = new Table(playerX, playerO);
    }

    public void run() {
        this.showWelcom();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWinner() == null) {
                    System.out.println("Draw!!");
                } else {
                    System.out.println(table.getWinner().getName() + " Win!!");
                }
                this.showTable();
                newGame();
            }
            table.switchPlayer();

        }

    }

}
